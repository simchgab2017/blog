<?php
session_start();
?>
<!doctype html>
<html lang = "en">
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport"
          content = "width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv = "X-UA-Compatible" content = "ie=edge">
    <title>Formulario Nuevo Post</title>
    <link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
  <body class="container">

<section class="row justify-content-center">
    <article class="col-8 table-bordered">
        <h6 class="text-center"><i class="fa fa-android" aria-hidden="true">Nuevo Post</i></h6>
        <form action = "nuevo_post.php" method="post" enctype="multipart/form-data">
            <div class = "form-group">
              <label for = "titulo">Titulo</label>
              <input type = "text" class = "form-control" name = "titulo" id = "titulo" aria-describedby = "helpId"
                     placeholder = "Ingrese un titulo">
            </div>

            <div class = "form-group">
              <label for = "comentarios">Comentarios</label>
              <textarea class = "form-control" name = "comentarios" id = "comentarios" rows = "3"></textarea>
            </div>

            <input type="hidden" name="MAX_FILE_SIZE" value="2097128">

            <div class="form-group">
                <label for="imagen" class="form-control-label">Imagen</label>
                <input type="file" class="form-control-file" name="imagen" id="imagen">
              <small id = "fileHelpId" class = "form-text text-muted">La imagen debe ser del formato .JPEG .JPG o .GIF
                                                                      no debe ocupar mas de 2MB</small>
            </div>

            <input type = "submit" class = "btn btn-primary btn-block" name="enviar">

            <div class="text-center">
                <a href = "ver_post.php" class = "btn-link"><strong>Ver Post</strong></a>
            </div>
        </form>
    </article>
</section>

<script src="js/bootstrap.min.js"></script>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity = "sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin = "anonymous"></script>
  </body>
</html>