<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: gabriel
 * Date: 19/1/2017
 * Time: 10:43
 */

namespace classes\database;

require ('confHost.php');


/**
 * Class Conexion
 * @package classes\database
 */
class Conexion
{
    /**
     * @return \PDO
     */
    public static function conectar()
    {
        try{
            $conexion = new \PDO(DRIVERHOST,USERDB,PASSDB);
            $conexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
            $conexion->exec(CHARACTERSET);
        }catch (\PDOException $error){
            echo '<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">';
            echo '<link rel="stylesheet" href="../../css/bootstrap.min.css">';
            echo "<link href='../../css/bootstrap.min.css'>";
            echo "<div class = \"alert alert-danger alert-dismissible fade\" role = \"alert\">";
            echo "<button type = \"button\" class = \"close\" data-dismiss = \"alert\" aria-label = \"Close\">";
            echo "<span aria-hidden = \"true\">&times;</span>";
            echo "<span class = \"sr-only\">Close</span>";
            echo "</button>";
            echo "<strong>Error: </strong>".$error->getMessage()."<strong> File: </strong>".$error->getFile()."<strong>
            Line: ".$error->getLine()."</strong></div>";

            echo '<script src="../../js/bootstrap.min.js"></script>';
            echo '<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>';
            echo '<script src = "https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>';
            echo '<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity = "sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin = "anonymous"></script>';

        }
        return $conexion;
    }
}