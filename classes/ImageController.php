<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: gabriel
 * Date: 19/1/2017
 * Time: 11:54
 */

namespace classes;


/**
 * Permite realizar
 * las verificaciones pertinentes a una imagen, tamaño valido, tipos de datos validos, verifica errores. y modificar el
 * nombre de la imagen si este ya se encuentra repetido en la carpeta de imagenes
 * Class Controller_img
 * @package classes
 */
class ImageController
{
    /**
     * @var
     */
    private $aFile;
    /**
     * @var
     */
    private $aMaxSize;
    /**
     * @var
     */
    private $aDestinationPath;

    /**
     * @var array
     */
    private $tiposImg;


    /**
     * ControllerIMG constructor.
     * @param $aFile
     * @param $aMaxSize
     * @param array $tiposImg deben ser del tipo IMAGETYPE_TIPO
     * @param $aDestinationPath
     * @throws \Exception
     */
    public function __construct($aFile,$aMaxSize,$tiposImg = [],$aDestinationPath){
        $error = $this->checkErrors();
        if ($error != true){
            throw new \Exception($error);
        }else{
            $this->aFile = $aFile;
            $this->aMaxSize = $aMaxSize;
            $this->tiposImg = $tiposImg;
            $this->aDestinationPath = $aDestinationPath;
        }
    }

    /**
     * Si no hay errores retorna true, caso contrario retorna un string con el error ocurrido
     * @return bool|string
     */
    public function checkErrors(){
        $error = true; //Por defecto no tiene errores
        if ($this->aFile['error']){
            switch ($this->aFile['error']){
                case 1: //El fichero subido excede la directiva upload_max_filesize de php.ini
                    $error = "La imagen excede el tamaño maximo soportado por el servidor";
                    break;

                case 2: //El fichero subido excede la directiva MAX_FILE_SIZE especificada en el formulario HTML.
                    $error = "La imagen excede los 2MB";
                    break;

                case 3: //El fichero fue sólo parcialmente subido.
                    $error = "El fichero fue sólo parcialmente subido.";
                    break;

                case 4: //No se subió ningún fichero.
                    $error = "No se subió ningún fichero.";
                    break;

                case 5: //Falta la carpeta temporal
                    $error = "Falta la carpeta temporal";
                    break;

                case 6: //No se pudo escribir el fichero en el disco.
                    $error = "No se pudo escribir el fichero en el servidor.";
                    break;

                case 7: //Una extensión de PHP detuvo la subida de ficheros
                    $error = "Una extensión de PHP detuvo la subida de ficheros";
                    break;
            }
        }
        return $error;
    }

    /**
     * Verifica que el nombre de la imagen no exista en el path del archivo, si no existe, lo deja igual de existir
     * Genera un nuevo nombre, manteniendo el nombre del archivo original + _ y 13 caracteres mas aleatorios.
     * @return bool
     */
    public function verifyUniqueName(){
        $existe = false;

        if (file_exists($this->aDestinationPath . $this->aFile['name'])){

            //En $nomImg guardo solo el nombre del archivo sin el tipo del mismo en $tipoArchivo guardo el tipo
            list($nom_img,$tipo_archivo) = explode(".",$this->aFile['name']);

            //Agrega _ luego del nombre de la imagen sin el tipo y 13 caracteres mas generando un nuevo nombre
            $uniqid = $nom_img."_".uniqid();

            //Guarda el nuevo nombre en el archivo junto a la extension del mismo
            $this->aFile['name'] = $uniqid.".".$tipo_archivo;
            $existe = true;
        }
        return $existe;
    }



    /**
     * Retorna true si la imagen es de alguno de los tipos expresados al instanciar el objeto controllerIMG
     * False en caso contrario
     * @return bool
     */
    public function verifyTypes(){
        $todo_ok = false;
        $source_path = $this->aFile['tmp_name'];
        foreach ($this->tiposImg as $tipo_img){
            if (exif_imagetype($source_path) === $tipo_img){
                $todo_ok = true;
            }
        }
        return $todo_ok;
    }

    /**
     * Verifica que la imagen tenga un tamaño = o inferior al maximo permitido devolviendo true si es asi
     * caso contrario false
     * @return bool
     */
    public function verifyMaxSize(){
        $todo_ok = false;
        if ($this->aFile['size'] <= $this->aMaxSize){
            $todo_ok = true;
        }
        return $todo_ok;
    }

    /**
     * * Si el tipo de imagen es correcto y el tamaño es <= que el tamaño maximo, verifica que el nombre sea unico en la
     * carpeta de destino, mueve la imagen a la carpeta de destino, y retorna el nuevo nombre de imagen en caso de haber
     * sido cambiado, para poder ser utilizado en una consulta de BD y agregar dicho nombre a la BD
     * @throws \Exception Si ocurre un fallo al mover la imagen a la carpeta destino
     * @return mixed Retorna el nuevo nombre del archivo si este ya existe
     */
    public  function uploadImage(){
        //Si el tipo es el correcto y es menor o = que el tamaño maximo
        if ($this->verifyTypes() && $this->verifyMaxSize()){
            //Mientras el nombre no sea unico lo modifico
            while ($this->verifyUniqueName()){
                $this->verifyUniqueName();
            }

            //Muevo la imagen al directorio del servidor
            move_uploaded_file($this->aFile['tmp_name'],$this->aDestinationPath.'/'.$this->aFile['name']);
        }else{
            throw new \Exception("Error al subir la imagen al servidor");
        }
        return $this->aFile['name'];
    }

    /**
     * @return mixed
     */
    public function getAFile()
    {
        return $this->aFile;
    }

    /**
     * @return mixed
     */
    public function getAMaxSize()
    {
        return $this->aMaxSize;
    }

    /**
     * @return mixed
     */
    public function getADestinationPath()
    {
        return $this->aDestinationPath;
    }

    /**
     * @return array
     */
    public function getTiposImg(): array
    {
        return $this->tiposImg;
    }


}