<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: gabriel
 * Date: 19/1/2017
 * Time: 11:56
 * @param $class
 */

spl_autoload_register(function ($class) {
    {

        //Prefijo de namespace especifico del proyecto
        $prefijo = 'classes';

        //Directorio base para el prefijo del namesapace
        $dir_base = __DIR__;

        //Verifica si la clase usa el prefijo de namespace
        $length = strlen($prefijo);
        if (strncmp($prefijo, $class, $length) !== 0) {
            //no se mueve al registro de autoload
            return;
        }

        //Toma el nombre de la clase relativa
        $relative_class = substr($class, $length);

        //Remplaza el prefijo de namespace con el directorio base
        //Remplaza los separadores \ con / en el nombre relativo de clase
        //Y agrega el .php
        $archivo = $dir_base.str_replace('\\', '/', $relative_class).'.php';
        //Si el archivo existe, hace el require
        if (file_exists($archivo)) {
            require $archivo;
        }
    }
}
);
