<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: gabriel
 * Date: 20/1/2017
 * Time: 08:56
 */

namespace classes;
require 'autoload.php';
use classes\database\Conexion;

define("DESTINATIONPATHIMAGE",$_SERVER['DOCUMENT_ROOT'].'blog/imagenes/');

/**
 * Class PostController
 * @package classes
 */
class PostController
{
    private $titulo;
    private $comentarios;
    private $date;
    private $file;
    private $typeFile = [];
    private $maxSize;

    /**
     * PostController constructor.
     * @param $titulo
     * @param $comentarios
     * @param $date
     * @param $file
     * @param array $typeFile
     * @param $maxSize
     */
    public function __construct($titulo, $comentarios, $date, $file, array $typeFile,$maxSize)
    {
        $this->titulo = $titulo;
        $this->comentarios = $comentarios;
        $this->date = $date;
        $this->typeFile = $typeFile;
        $this->file = $file;
        $this->maxSize = $maxSize;
    }

    /**
     * Retorna una excepcion en caso de error, en caso de exito retorna un mensaje de exito
     * @return string
     * @throws \Exception
     */
    public function uploadImage()
    {
        $imagen = new ImageController($this->file,$this->maxSize,[IMAGETYPE_GIF,IMAGETYPE_JPEG],
          DESTINATIONPATHIMAGE);

        $this->file['name'] = $imagen->uploadImage();

        $query = "INSERT INTO bdblog.contenido (Titulo, Fecha, Comentario, Imagen) VALUES (:tit,:fecha,:comen,:img)";
        $connect = Conexion::conectar();
        $result = $connect->prepare($query);
        $result->bindValue(":tit",$this->titulo);
        $result->bindValue(":fecha",$this->date);
        $result->bindValue(":comen",$this->comentarios);
        $result->bindValue(":img",$this->file['name']);
        $result->execute();
        if (!$result){
            echo $connect->errorInfo();
        }
        $connect = null;
        return true;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * @param mixed $comentarios
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return array
     */
    public function getTypeFile(): array
    {
        return $this->typeFile;
    }

    /**
     * @param array $typeFile
     */
    public function setTypeFile(array $typeFile)
    {
        $this->typeFile = $typeFile;
    }

    /**
     * @return mixed
     */
    public function getMaxSize()
    {
        return $this->maxSize;
    }

    /**
     * @param mixed $maxSize
     */
    public function setMaxSize($maxSize)
    {
        $this->maxSize = $maxSize;
    }


}