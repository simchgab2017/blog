<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: gabriel
 * Date: 19/1/2017
 * Time: 11:13
 */



include "./classes/autoload.php";
try {
    //Si no se presiono el boton de enviar lanza excepcion
    if (!isset($_POST['enviar'])) {
        throw new Exception("Error no se envio el formulario");
        //Valido el titulo no sea valido
    } elseif ((!isset($_POST['titulo'])) || (trim($_POST['titulo']) == "") || (is_numeric($_POST['titulo']))) {
        throw new Exception("Por favor verifique el titulo del post");
        //valido los comentarios sean validos
    } elseif ((!isset($_POST['comentarios'])) || (trim($_POST['comentarios']) == "") ||
      (is_numeric($_POST['comentarios']))
    ) {
        throw new Exception("Por favor verifique los comentarios");
        //Valido este seleccionada la imagen
    } elseif (strlen($_FILES['imagen']['name']) == 0) {
        throw new Exception("Por favor seleccione una imagen");
    } else {

        $post = new \classes\PostController(
          $_POST['titulo'], $_POST['comentarios'], date("Y-m-d H-i-s"),
          $_FILES['imagen'], [IMAGETYPE_GIF, IMAGETYPE_JPEG], $_POST['MAX_FILE_SIZE']
        );
        if ($post->uploadImage()) {
            echo '<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5
/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" 
crossorigin="anonymous">';
            echo '<div class = "alert alert-success alert-dismissible fade in" role = "alert">';
            echo '<button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">';
            echo '<span aria-hidden = "true">&times;</span>';
            echo '<span class = "sr-only">Close</span>';
            echo '</button>';
            echo '<strong>Felicitaciones: </strong> Su Post fue creado correctamente';
            echo '</div>';
            echo '<script src="js/bootstrap.min.js"></script>';
            echo '<script src = "https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>';
            echo '<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>';
            echo '<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity = "sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin = "anonymous"></script>';

        }

    }
}catch (Exception $error){

    echo '<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5
/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" 
crossorigin="anonymous">';

    echo '<div class = "alert alert-danger alert-dismissible fade in" role = "alert">';
    echo '<button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">';
    echo '<span aria-hidden = "true">&times;</span>';
    echo '<span class = "sr-only">Close</span>';
    echo '</button>';
    echo '<strong>Error:  </strong>'.$error->getMessage().'<strong> File: </strong>'.$error->getFile();
    echo '<strong> Line: </strong>'.$error->getLine();
    echo '</div>';

    echo'<script src="js/bootstrap.min.js"></script>';
    echo'<script src = "https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>';
    echo '<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>';
    echo '<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity = "sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin = "anonymous"></script>';
}

